var canvas = document.querySelector('#logo-40 canvas');



var detail = {
    icon:{
        src:'./svg/icon-3.svg',
        x:186,
        y:84,
        width:192,
        height:164,
        color_1: '#111111',
        color_2: '#A1575C',
        color_3: '#C11427',
        color_4: '#45171A'
    },
    title:{
        family:'Grand Hotel',
        isBold: false,
        isItalic: false,
        size: 68.7539,
        color: '#45171A',
        line_1:{
            text:'La Luna',
            x: 190,
            y: 264.40625
        }
        // Optional Line 2
        // ,
        // line_2:{
        //     text: 'Maurice',
        //     x: 207.828125,
        //     y: 304.40625
        // }
    },
    tagline:{
        color: '#444',
        text: 'Votre guide de coiffure et de beauté',
        isBold: true,
        isItalic: true,
        family: 'Chivo',
        size: 16.0286,
        x: 145,
        y: 356.40625
    }
}

var actual = {
    width:560,
    height:454
}


var getHeightbyRatio = function(widthTarget){
    return actual.height / actual.width * widthTarget;
}

var setDimension = function(ctx, actual, target){
    let screen = {
        width: target,
        height: getHeightbyRatio(target)
    };
    

    ctx.canvas.width = screen.width;
    ctx.canvas.height = screen.height;
}

var drawCanvas = function(el, detail, actual, target){
    var ctx = canvas.getContext('2d');
    
    // Canvas Dimension
    setDimension(ctx, actual, target);

    var targetWidth = ctx.canvas.width;
    var actualWidth = actual.width;

    // Full size
    let title = detail.title;
    let line_1 = detail.title.line_1;
    let line_2 = detail.title.line_2;

    let title_1 = {
        ...line_1,
        ...title
    }
    let title_2 = {
        ...line_2,
        ...title
    }
    delete title_1.line_1;
    delete title_1.line_2;

    delete title_2.line_1;
    delete title_2.line_2;

    generateSVG(ctx, detail.icon, actualWidth, targetWidth);
    generateText(ctx, title_1, actualWidth, targetWidth);
    generateText(ctx, title_2, actualWidth, targetWidth);
    generateText(ctx, detail.tagline, actualWidth, targetWidth);
}
var generateLogo = function(el, detail, actual, screen){  
    WebFont.load({
        google: {
            families: [detail.title.family, detail.tagline.family]
        },
        active: function () {
            drawCanvas(el, detail, actual, screen)
        }
    });
}

var scale = function(size, actual, target){
    return (target / actual) * size;
}

var convertSVGBase64 = function(idselector){
    var target = document.querySelector('[name="'+idselector+'"]');
    var svg = document.querySelector('[name="' + idselector + '"] svg');

    var s = new XMLSerializer().serializeToString(svg);
    var base64 = 'data:image/svg+xml;base64,'+window.btoa(s);
    target.remove();

    return base64;
}
var coloringImage = function(idcontainer, iconDetail){
    let selector_color_1 = document.querySelectorAll('[name="' + idcontainer + '"] .color_1');
    let selector_color_2 = document.querySelectorAll('[name="' + idcontainer + '"] .color_2');
    let selector_color_3 = document.querySelectorAll('[name="' + idcontainer + '"] .color_3');
    let selector_color_4 = document.querySelectorAll('[name="' + idcontainer + '"] .color_4');
    
    selector_color_1.forEach(function (innerSelector, i) {
        innerSelector.removeAttribute('fill');
        innerSelector.style.fill = iconDetail.color_1;
    });
    selector_color_2.forEach(function (innerSelector, i) {
        innerSelector.removeAttribute('fill');
        innerSelector.style.fill = iconDetail.color_2;
    });
    selector_color_3.forEach(function (innerSelector, i) {
        innerSelector.removeAttribute('fill');
        innerSelector.style.fill = iconDetail.color_3;
    });
    selector_color_4.forEach(function(innerSelector, i){
        innerSelector.removeAttribute('fill');
        innerSelector.style.fill = iconDetail.color_4;
    });

    return convertSVGBase64(idcontainer);
}
var loadImage = function(ctx, iconDetail){
    let img = new Image();

    return new Promise(function (resolve, reject) {
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function(){
            if(this.readyState == 4 && this.status == 200){
                // console.log(this.response);
                let idcontainer = ctx.canvas.parentElement.getAttribute('id');
                let doc = new DOMParser().parseFromString('<div name="'+idcontainer+'">'+this.response+'</div>', 'text/html');
                document.querySelector('body').appendChild(doc.body.firstChild);

                let baseImage = coloringImage(idcontainer, iconDetail);
                
                img.src = baseImage;
                img.onload = function (res) {
                    resolve(img);
                };
            }
        }
        xhttp.open('GET', iconDetail.src, true);
        xhttp.send();
    });
}
var generateSVG = function(ctx, iconDetail, actual, screen){

    let x = scale(iconDetail.x, actual, screen);
    let y = scale(iconDetail.y, actual, screen);
    let width = scale(iconDetail.width, actual, screen);
    let height = scale(iconDetail.height, actual, screen);

    loadImage(ctx, iconDetail).then(function (obj) {
        ctx.drawImage(obj, x, y, width, height)
    });
}

var generateText = function(ctx, fontDetail, actual, screen){


    let x = scale(fontDetail.x, actual, screen);
    let y = scale(fontDetail.y, actual, screen);
    let size = scale(fontDetail.size, actual, screen);

    let font = '';
    font += fontDetail.isBold == true ? 'bold ' : '';
    font += fontDetail.isItalic == true ? 'italic ' : '';
    font += fontDetail.size ? size + 'px' : '';
    font += fontDetail.family ? ' ' + fontDetail.family : '';

    console.log(font);

    ctx.font = font;
    ctx.fillStyle = fontDetail.color;
    ctx.fillText(fontDetail.text, x, y + size);
}


// Canvas node, detail, actual size, actual screen
generateLogo(canvas, detail, actual, 760);