const express = require('express');
const app = express();

app.get('/', (req, res) => res.send('Hello World'));
app.get('/home', (req, res) => res.send('Home'));
app.get('/profile', (req, res) => res.send('Profile'));


app.listen(2017, () => console.log("Run on Port 2017"));